<?php

/**
 * Easythumb settings form
 */
function easythumb_settings() {
  $form = array();

  $form['easythumb_bluga_user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('User ID'),
    '#default_value' => variable_get('easythumb_bluga_user_id', ''),
    '#required' => TRUE,
    '#description' => t('Your Bluga Webthumb user_id number available on the "api details" section of your !link.', array('!link' => l(t('user page'), 'http://webthumb.bluga.net/user', array('attributes' => array('target' => '_blank'))))),
  );

  $form['easythumb_bluga_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('easythumb_bluga_api_key', ''),
    '#required' => TRUE,
    '#description' => t('Your Bluga Webthumb API key available on the "api details" section of your !user_link. If you don\'t have one yet, you can get one !link.', array('!user_link' => l(t('user page'), 'http://webthumb.bluga.net/user', array('attributes' => array('target' => '_blank'))), '!link' => l(t('here'), 'http://webthumb.bluga.net/register', array('attributes' => array('target' => '_blank'))))),
  );

  $form['easythumb_debug'] = array(
    '#type' => 'select',
    '#title' => t('Debug'),
    '#options' => array(0 => t('Disabled') , 1 => t('Enabled') ),
    '#default_value' => variable_get('easythumb_debug', 0),
    '#description' => t('Debugging logs.'),
  );

  return system_settings_form($form);
}
